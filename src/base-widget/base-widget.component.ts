import { Component, OnInit } from '@angular/core';
declare var jQuery:any;
declare var carousel:any;
@Component({
  selector: 'app-base-widget',
  template: '<div class="container"><div class="card-block"><div class="col-md-12"><div class="col-md-4"><span class="tag tag-primary">Positives</span><div class="streaming-news-timeline"><div class="list-group list-news"><span class="list-group-item" *ngFor="let item of Positives"><div class="content-tweet"><a target="_blank" href="https://twitter.com/bxlley_"><img class="profile-picture" src="assets/img/noimg.png"><img class="profile-picture" onerror="this.style.display='+'none'+'" src="{{item.image}}"><strong class="tweets-name">{{item.name}}</strong>  &nbsp;<span class="text-muted">{{item.media_displayName}}</span><br></a><a class="text-muted post-date" target="_blank">{{item.timestamp}}</a><a target="_blank"><p>  {{item.description | slice:0:100}} ...</p></a></div></span><span *ngIf="SlideComponent?.length == 0"><p style="text-align: center;"><b>No Result</b></p></span></div></div></div><div class="col-md-4"><span class="tag tag-default">Neutrals</span><div class="streaming-news-timeline"><div class="list-group list-news"><span class="list-group-item" *ngFor="let item of Neutrals"><div class="content-tweet"><a target="_blank" href="https://twitter.com/bxlley_"><img class="profile-picture" src="assets/img/noimg.png"><img class="profile-picture" onerror="this.style.display='+'none'+'" src="{{item.image}}"><strong class="tweets-name">{{item.name}}</strong> &nbsp;<span class="text-muted">{{item.media_displayName}}</span><br></a><a class="text-muted post-date" target="_blank">{{item.timestamp}}</a><a target="_blank"><p>  {{item.description | slice:0:100}} ...</p></a></div></span><span *ngIf="SlideComponent?.length == 0"><p style="text-align: center;"><b>No Result</b></p></span></div></div></div><div class="col-md-4"><span class="tag tag-danger">Negatives</span><div class="streaming-news-timeline"><div class="list-group list-news"><span class="list-group-item" *ngFor="let item of Negatives"><div class="content-tweet"><a target="_blank" href="https://twitter.com/bxlley_"><img class="profile-picture" src="assets/img/noimg.png"><img class="profile-picture" onerror="this.style.display='+'none'+'" src="{{item.image}}"><strong class="tweets-name">{{item.name}}</strong> &nbsp;<span class="text-muted">{{item.media_displayName}}</span><br></a><a class="text-muted post-date" target="_blank">{{item.timestamp}}</a><a target="_blank"><p>  {{item.description | slice:0:100}} ...</p></a></div></span><span *ngIf="SlideComponent?.length == 0"><p style="text-align: center;"><b>No Result</b></p></span></div></div></div></div></div></div>',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
public Positives: Array<any> = [
 {
  "name": "ayu 1",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": ""
 },
 {
  "name": "ayu 2",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "./assets/img/noimg.png"
 },
 {
  "name": "ayus",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 },
 {
  "name": "ayunda",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": ""
 },
 {
  "name": "ayu45",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 },
 {
  "name": "ayuer",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 }
 
 
];
public Neutrals: Array<any> = [
 {
  "name": "ayu 1",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": ""
 },
 {
  "name": "ayu 2",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "./assets/img/noimg.png"
 },
 {
  "name": "ayus",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 },
 {
  "name": "ayunda",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": ""
 },
 {
  "name": "ayu45",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 },
 {
  "name": "ayuer",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 }
 
 
];
public Negatives: Array<any> = [
 {
  "name": "ayu 1",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": ""
 },
 {
  "name": "ayu 2",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "./assets/img/noimg.png"
 },
 {
  "name": "ayus",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 },
 {
  "name": "ayunda",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": ""
 },
 {
  "name": "ayu45",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 },
 {
  "name": "ayuer",
  "timestamp": "2 days ago",
  "media_displayName": "detik.com",
  "description": "Jakarta - Pemungutan suara Pilkada Jakarta tinggal dua hari lagi. Ketua Umum PBNU KH Said Aqil Siroj mengatakan situasi yang sedang memanas harus disikapi dengan bijaksana Setelah mengamati situasi beberapa hari ini baik di seluruh daerah maupun khususnya di Jakarta ada seperti yang disebutkan agak memanas. ",
  "image": "http://www.newscaststudio.com/wp-content/uploads/2016/11/nbc-news-logo.png"
 }
 
 
];
  constructor() { }

  ngOnInit() {
    // console.log(this.SlideComponent);

  }

}